import datetime as dt

import numpy as np
import pandas as pd

np.random.seed(5)  # keep it random but reproducible


def create_meter_data():
    """Creates random, continues Mock Home Meter consumption values in Watts for a whole day;
    one value per second.
    """
    data = []
    max_watts = 9000

    sum = 0
    for x in range(200):
        value = np.random.uniform(
            -800, 800
        )  # determines how much data changes in the interval
        if value <= 0 and value + sum <= 0:
            value = -value
        if value + sum >= max_watts:
            value = -value
        sum += value
        data.append(value)
    sample_values = len(data)

    xs = np.array([i for i in range(0, sample_values)])

    start_of_day = dt.datetime.combine(dt.date.today(), dt.datetime.min.time())

    index = pd.Index(
        start_of_day
        + xs
        * pd.offsets.Second(
            432
        )  # offset so that we have one value per seconds of the day
    )
    df = pd.DataFrame(data, index=index, columns=["watts"])
    df = df.cumsum()

    upsampled = df.resample("S").interpolate(method="cubic")
    return upsampled
