import pika
from common.messaging import EXCHANGE, QUEUE, ROUTING_KEY

# def open_channel():
#     credentials = pika.PlainCredentials("guest", "guest")
#     conn_params = pika.ConnectionParameters("tmh_broker_1", credentials=credentials)
#     conn_broker = pika.BlockingConnection(conn_params)
#     channel = conn_broker.channel()
#     channel.exchange_declare(exchange=EXCHANGE, passive=False, durable=True, auto_delete=False)
#     return channel


def publish(channel, msg):
    msg_props = pika.BasicProperties()
    msg_props.content_type = "text/plain"
    channel.basic_publish(
        body=msg, exchange=EXCHANGE, properties=msg_props, routing_key=ROUTING_KEY
    )
