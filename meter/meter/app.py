import datetime as dt
from time import sleep

from common.messaging import EXCHANGE, EXCHANGE_TYPE, Message, retry_for_channel
from messaging import publish

from meter.mock_data import create_meter_data

if __name__ == "__main__":
    mock_data = create_meter_data()

    with retry_for_channel() as producer_channel:
        producer_channel.exchange_declare(
            exchange=EXCHANGE, exchange_type=EXCHANGE_TYPE
        )
        while True:
            now = dt.datetime.now().replace(microsecond=0)
            current_consumption = mock_data.loc[now].at["watts"]

            m = Message(timestamp=dt.datetime.now(), consumption=current_consumption)
            print(f"sending message {m}")
            publish(producer_channel, m.json())
            sleep(1)
