""" common code between meter and pv-sim app, this package  needs to be kept in sync, but could also
 be moved to a common library """
import datetime as dt
from contextlib import contextmanager
from time import sleep

import pika
from pydantic import BaseModel, Field

HOST = "broker"
EXCHANGE = "meter-readings"
EXCHANGE_TYPE = "fanout"
STOP_MESSAGE = "quit"
QUEUE = ""

ROUTING_KEY = ""


class Message(BaseModel):
    timestamp: dt.datetime = Field(description="UTC  datetime")
    consumption: float = Field(
        default=0, description="Home meter consumption in Watts", ge=0
    )


@contextmanager
def retry_for_channel(host=HOST):
    """Wait for RabbitMQ to start and create a channel. Close the connection afterwards"""
    channel = None
    while not channel:
        try:
            credentials = pika.PlainCredentials("guest", "guest")
            conn_params = pika.ConnectionParameters(host, credentials=credentials)
            conn_broker = pika.BlockingConnection(conn_params)
            channel = conn_broker.channel()

        except pika.exceptions.AMQPConnectionError:
            print("waiting for RabbitMQ to start ")
            sleep(1)
    yield channel
    send_stop(channel)
    conn_broker.close


def send_stop(channel):
    """Sending this stops the consumer thread"""
    msg_props = pika.BasicProperties()
    msg_props.content_type = "text/plain"
    channel.basic_publish(
        body=STOP_MESSAGE,
        exchange=EXCHANGE,
        properties=msg_props,
        routing_key=ROUTING_KEY,
    )
