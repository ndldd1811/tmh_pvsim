#!/usr/bin/env bash
# Helper script for setting up a local development environment and for testing it.
# you can also run `source dev.sh` to keep the newly created venv activated and run the commands interactively

VENV_DIR="venv"
py_3="$VENV_DIR/bin/python3"

setup() {

  if [[ -d "$VENV_DIR" ]]; then
    echo "$VENV_DIR directory exists already."
  else
    virtualenv -p python3 $VENV_DIR

  fi

  source "$VENV_DIR/bin/activate"
  $py_3 --version
  $py_3 -m pip install -r pv-sim/requirements.txt
  $py_3 -m pip install -r meter/requirements.txt
  $py_3 -m pip install -r requirements_dev.txt

  $py_3 -m pip install -e pv-sim
  $py_3 -m pip install -e meter

}

test() {
  pushd pv-sim
  ../$py_3 -m pytest tests/unit
  echo "running integration tests"
  ../$py_3 -m pytest tests/integration
  popd
}


# run all commands or just execute the supplied function
if [ -z "$1" ]; then
  setup
  test
  else
      $1
fi