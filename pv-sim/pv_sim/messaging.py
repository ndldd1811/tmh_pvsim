from queue import Queue

import pika
from common.messaging import (
    EXCHANGE,
    EXCHANGE_TYPE,
    HOST,
    QUEUE,
    STOP_MESSAGE,
    Message,
    retry_for_channel,
)
from pydantic import BaseModel, Field

CONSUMER_TAG = "pv-sim"


class MeterValue(BaseModel):
    """Used for sharing meter values between the MeterListener thread and the PvSimulator thread"""

    time: int = Field(..., description="Time as second of the day", ge=0)
    meter: float = Field(..., description="Power consumption in Watt")


def consume_messages(queue: Queue, rabbit_queue: str = None, host: str = HOST) -> None:
    """Subscribe to the rabbit queue. For each received Message
    return a MeterValue and share it with the PvSimulator Thread via the Queue"""

    print(f"Consuming messages on Queue: {rabbit_queue}")

    def msg_consumer(channel, method, header, body):
        channel.basic_ack(delivery_tag=method.delivery_tag)
        if body.decode("utf-8") == STOP_MESSAGE:
            channel.basic_cancel(consumer_tag=CONSUMER_TAG)
            channel.stop_consuming()
        else:
            msg = Message.parse_raw(body.decode("utf-8"))
            seconds_since_midnight = (
                msg.timestamp
                - msg.timestamp.replace(hour=0, minute=0, second=0, microsecond=0)
            ).total_seconds()
            seconds_since_midnight = round(seconds_since_midnight)
            meter_value = MeterValue(
                time=seconds_since_midnight, meter=msg.consumption / 1000
            )  # convert Watt to kW
            queue.put(meter_value)

    with retry_for_channel(host=host) as consumer_channel:
        consumer_channel.exchange_declare(
            exchange=EXCHANGE, exchange_type=EXCHANGE_TYPE
        )
        result = consumer_channel.queue_declare(queue=QUEUE, exclusive=True)
        consumer_channel.queue_bind(exchange=EXCHANGE, queue=result.method.queue)
        consumer_channel.basic_consume(
            queue=rabbit_queue,
            consumer_tag=CONSUMER_TAG,
            on_message_callback=msg_consumer,
        )
        consumer_channel.start_consuming()


def create_connection_and_get_channel(conn_params, rabbit_queue):
    """connection should be created in the same thread were the callback runs"""
    conn_broker = pika.BlockingConnection(conn_params)
    channel = conn_broker.channel()
    return channel
