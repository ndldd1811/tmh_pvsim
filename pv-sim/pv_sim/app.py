import threading
import time
from queue import Queue
from typing import Dict

import pandas as pd
from common.messaging import HOST, ROUTING_KEY
from pv_sim.combine import combiner
from pv_sim.messaging import consume_messages
from pv_sim.mock_data import create_mock_data
from pv_sim.pv_logging import append_to_log_file

mock_pv_data = create_mock_data()


class StoppableThread(threading.Thread):
    def __init__(self, queue, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._stop_event = threading.Event()
        self.queue = queue

    def stop(self):
        self._stop_event.set()


class PvSimulator(StoppableThread):
    """Connects via a thread-safe Queue with the MeterListener to receive MeterValues.
    The MeterValues are combined with simulated Photovoltaic Values readings and logged to the LOG_FILE"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.buffer: Dict = {}
        self.mock_data: pd.DataFrame = create_mock_data()
        self.logs = []

    def run(self):
        while not self._stop_event.is_set():
            new_logs = combiner(
                meter_queue=self.queue, data=self.mock_data, buffer=self.buffer
            )
            self.logs.extend(new_logs)  # TODO:for easier testing
            for l in new_logs:
                append_to_log_file(l)
            time.sleep(1)


class MeterListener(StoppableThread):
    """Subscribes to the RabbitMQ Queue of the Meter Service and
    sends the received MeterValues to the PvSimulator Thread"""

    def __init__(self, host: str, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.host = host

    def run(self):
        print("MeterListener starting to receive messages")
        while not self._stop_event.is_set():
            consume_messages(self.queue, rabbit_queue=ROUTING_KEY, host=self.host)


def run_app():
    q = Queue()
    p = PvSimulator(queue=q)
    m = MeterListener(host=HOST, queue=q)
    m.start()
    p.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        p.stop()
        m.stop()


if __name__ == "__main__":
    run_app()
