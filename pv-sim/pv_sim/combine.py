import datetime as dt
import random
from queue import Queue
from typing import Dict, List, Tuple

from common.messaging import Message
from pandas import DataFrame
from pv_sim.pv_logging import LogValue


def get_time() -> int:
    now = dt.datetime.now()
    seconds_since_midnight = (
        now - now.replace(hour=0, minute=0, second=0, microsecond=0)
    ).total_seconds()
    return round(seconds_since_midnight)


def make_message(consumption=None):
    if consumption is None:
        consumption = random.uniform(0, 9000)
    m = Message(timestamp=dt.datetime.now(), consumption=consumption)
    return m


def get_pv_value(time_stamp: int, data: DataFrame) -> float:
    """Get the current power output of the PV system in kW"""
    try:
        val = data.loc[data["time"] == time_stamp]["kW"].item()
        return val
    except ValueError:
        """missing timestamp"""
        return None


BUFFER_THRESHOLD_SECONDS = 4


def combiner(
    meter_queue: Queue = None, data: DataFrame = None, buffer: Dict = None
) -> List[LogValue]:
    """Queries the current PV value each second and checks if we have received a meter value that we can combine with it.
    In case we do not have a meter value we hold the value back in a buffer.
    We check again on the next iteration whether a value has arrived in the meantime.
    If BUFFER_THRESHOLD_SECONDS is exceeded a log is created with the PV value and a Null meter value
    """

    now = get_time()
    pv = get_pv_value(now, data=data)
    logs = []

    _remove_too_old_values_from_buffer(buffer, logs, now)

    if meter_queue.qsize() == 0:
        # we don't have a meter message, lets wait some time before logging it as a Null
        _add_pv_value_to_buffer(buffer, now, pv)

    else:
        # We received new MeterValues to process"""
        found_current = False
        while not meter_queue.empty():
            meter_time, meter_value = get_next_meter_values(meter_queue)

            if meter_time < now:
                if buffer.get(
                    meter_time
                ):  # check if we have a buffered PV value for this meter value
                    handle_buffered_values(buffer, logs, meter_time, meter_value)
                else:
                    # ignore this case
                    pass
            elif meter_time == now:
                handle_buffered_values(buffer, logs, meter_time, meter_value)
                _log_directly(logs, meter_time, meter_value, pv)
                found_current = True
            elif meter_time > now:
                raise RuntimeError(
                    f"Received meter value with invalid timestamp: {meter_time} at {now} "
                )
        if not found_current:
            _add_pv_value_to_buffer(buffer, now, pv)
    return logs


def get_next_meter_values(meter_queue: Queue) -> Tuple[int, float]:
    i = meter_queue.get()
    return i.time, i.meter


def _add_pv_value_to_buffer(buffer: Dict, now, pv) -> None:
    buffer[now] = pv


def _log_directly(logs, meter_time, meter_value, pv):
    sum = pv - meter_value
    meter_value = meter_value
    lv = LogValue(time=meter_time, pv=pv, sum=sum, meter=meter_value)
    logs.append(lv)


def handle_buffered_values(buffer, logs, meter_time, meter_value):
    """Check if we can log one of PvValues that we held back with a newly arrived MeterValue"""
    time_stamps = sorted(buffer.keys())
    for buffer_time in time_stamps:
        if buffer_time < meter_time:
            # The current meter value is newer than this buffer value, we wont receive a message for it.
            # discard the buffer value"""
            lv = LogValue(
                time=buffer_time, pv=buffer[buffer_time], sum=None, meter=None
            )
            buffer.pop(buffer_time)
            logs.append(lv)
        elif buffer_time == meter_time:
            lv = LogValue(
                time=buffer_time,
                pv=buffer[buffer_time],
                sum=buffer[buffer_time] - meter_value,
                meter=meter_value,
            )
            buffer.pop(buffer_time)
            logs.append(lv)


def _remove_too_old_values_from_buffer(buffer, logs, now):
    if buffer is not None:
        time_stamps = sorted(buffer.keys())
        for k in time_stamps:
            if k <= now - BUFFER_THRESHOLD_SECONDS:
                v = buffer.pop(k)
                lv = LogValue(time=k, pv=v, sum=None, meter=None)
                logs.append(lv)
