import datetime as dt

import numpy as np
from pandas import DataFrame
from scipy.interpolate import interp1d

# keep it data random but deterministic
np.random.seed(1)


def tts(h=0, m=0):
    """converts clock time to timesstamp"""
    return int(dt.timedelta(hours=h, minutes=m).total_seconds())


def create_pv_values(sample_x, sample_y):
    """Creates Mock Photovoltaic output data fitted to sample data"""
    sample_x_ts = [tts(x) for x in sample_x]
    assert len(sample_x_ts) == len(sample_y)

    f = interp1d(sample_x_ts, sample_y, kind="cubic")
    x_new = np.linspace(0, tts(h=24), num=tts(h=24), endpoint=True)
    y_new = f(x_new)

    left = tts(6)

    x = left
    while x < tts(24):
        y_new[left:x] = y_new[left:x] + np.random.uniform(
            0, 0.06
        )  # randomizing the curve

        left = x
        x += np.random.randint(600)  # random interval length

    y_new[0 : tts(h=5)] = 0
    y_new[tts(h=20, m=30) : tts(24)] = 0

    return x_new, y_new


def create_mock_data():
    # hour, kW
    sample_data = [
        (0, 0),
        (2, 0),
        (4, 0),
        (5, 0),
        (6, 0.1),
        (8, 0.4),
        (10, 2),
        (12, 2.9),
        (14, 3.25),
        (16, 3),
        (18, 2),
        (19, 1.2),
        (20, 0.2),
        (20.5, 0.0),
        (24, 0),
    ]
    sx, sy = zip(*sample_data)

    x_new, y_new = create_pv_values(sx, sy)
    data = DataFrame(data={"time": x_new, "kW": y_new})
    data["time"] = data["time"].astype("int32")
    return data
