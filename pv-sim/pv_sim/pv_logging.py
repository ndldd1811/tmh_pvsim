import datetime as dt
import os
from typing import Optional

from pydantic import BaseModel, Field

LOG_FILE = "pv-sim.logs"


class LogValue(BaseModel):
    time: int = Field(..., description="Seconds since midnight", ge=0, lt=24 * 60 * 60)
    pv: float = Field(..., description="Photovoltaic output in kW", ge=0)
    sum: Optional[float] = Field(
        default=None, description="pv output minus meter consumption in kW"
    )
    meter: Optional[float] = Field(default=None, description="meter consumption in kW")


def get_today() -> dt.datetime:
    return dt.datetime.today()


def append_to_log_file(log: LogValue) -> None:
    log_file = os.path.join(os.path.abspath(os.environ.get("LOG_DATA_DIR")), LOG_FILE)
    today_start = get_today().replace(hour=0, minute=0, second=0, microsecond=0)

    timestamp = today_start + dt.timedelta(seconds=log.time)
    line = f"{str(timestamp)} sum={log.sum}, pv={log.pv}, meter={log.meter}\n"
    print(line, end="")

    with open(log_file, "a+") as f:
        f.write(line)
