import os

import pytest
from pv_sim.pv_logging import LOG_FILE


@pytest.fixture()
def log_dir(tmpdir):
    original = os.environ.get("LOG_DATA_DIR")
    os.environ["LOG_DATA_DIR"] = str(tmpdir)
    yield tmpdir
    if original:
        os.environ["LOG_DATA_DIR"] = original


@pytest.fixture
def log_file(log_dir):
    file = os.path.join(log_dir, LOG_FILE)
    yield file
