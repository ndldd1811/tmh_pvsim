import datetime as dt
import os
from unittest.mock import patch

from pv_sim.pv_logging import LogValue, append_to_log_file


def test_log_file_writes(log_file):
    append_to_log_file(LogValue(time=0, pv=0, sum=0, meter=123))
    append_to_log_file(LogValue(time=1, pv=0, sum=0, meter=124))

    assert os.path.isfile(log_file)

    with open(log_file, "r") as f:
        content = f.read()
        assert "123" in content
        assert "124" in content


@patch("pv_sim.pv_logging.get_today")
def test_log_format(today, log_file):
    today.return_value = dt.datetime(
        2021, month=10, day=1, hour=3, minute=2, second=59, microsecond=123
    )
    lv = LogValue(time=11233, pv=2.4, sum=2.4, meter=2.0)

    append_to_log_file(lv)

    with open(log_file, "r") as f:
        content = f.read()
        assert content == "2021-10-01 03:07:13 sum=2.4, pv=2.4, meter=2.0\n"
