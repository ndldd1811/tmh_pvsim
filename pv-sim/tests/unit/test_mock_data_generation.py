from pandas.core.dtypes.common import is_integer_dtype
from pv_sim.combine import get_pv_value
from pv_sim.mock_data import create_mock_data


def test_mock_data():
    data = create_mock_data()


def test_get_data_from_mock_data():
    data = create_mock_data()
    get_pv_value(123, data)


def test_get_data_from_mock_data():
    data = create_mock_data()
    assert is_integer_dtype(data["time"])
