from queue import Queue
from unittest.mock import patch

import pytest
from pandas import DataFrame
from pv_sim.combine import BUFFER_THRESHOLD_SECONDS, combiner, get_pv_value
from pv_sim.messaging import MeterValue
from pv_sim.mock_data import tts
from pv_sim.pv_logging import LogValue


def queue_from_list(li):
    q = Queue()
    current = li[0].time
    for l in li:
        assert l.time >= current
        current = l.time
    for l in li:
        q.put(l)
    return q


def get_first_time_stamp(pv_data):
    return pv_data.loc[0]["time"]


@pytest.fixture()
def pv_data() -> DataFrame:
    """
    Sample of pv mock data for unit testing
    """
    timestamp = tts(h=12, m=4)
    data = [
        {"time": timestamp, "kW": 3.5},
        {"time": (timestamp + 1), "kW": 3.6},
        {"time": (timestamp + 2), "kW": 3.7},  # most recent value
    ]

    df = DataFrame(data=data)

    return df


def test_get_pv_value_returns_demo_data_value(pv_data):
    ts = tts(h=12, m=4)

    assert isinstance(get_pv_value(ts, pv_data), float)


@patch("pv_sim.combine.get_time")
def test_combine_value_without_meter_value_buffers(get_time, pv_data):
    ts = get_first_time_stamp(pv_data)
    get_time.return_value = ts

    meter_queue = Queue()
    buffer = {}

    logs = combiner(meter_queue, data=pv_data, buffer=buffer)

    assert logs == []
    assert len(buffer.keys()) == 1


@patch("pv_sim.combine.get_time")
def test_combiner_without_current_meter_value_retrieves_old_value_from_buffer(
    get_time, pv_data
):
    ts = get_first_time_stamp(pv_data)
    get_time.return_value = ts

    meter_queue = Queue()
    buffer = {(ts - BUFFER_THRESHOLD_SECONDS): 3.5}

    logs = combiner(meter_queue, data=pv_data, buffer=buffer)

    assert logs == [
        LogValue(time=ts - BUFFER_THRESHOLD_SECONDS, pv=3.5, sum=None, meter=None)
    ]
    assert len(buffer.keys()) == 1


@patch("pv_sim.combine.get_time")
def test_combiner_with_current_meter_value(get_time, pv_data):
    ts = get_first_time_stamp(pv_data)
    get_time.return_value = ts

    meter_queue = queue_from_list([MeterValue(time=ts, meter=2.0)])

    logs = combiner(meter_queue, data=pv_data, buffer={})

    assert logs == [LogValue(time=ts, pv=3.5, sum=1.5, meter=2)]


@patch("pv_sim.combine.get_time")
def test_combiner_buffers_with_empty_meter_queue(get_time, pv_data):
    ts = get_first_time_stamp(pv_data)
    get_time.return_value = ts

    meter_queue = Queue()
    buffer = {}
    logs = combiner(meter_queue, data=pv_data, buffer=buffer)

    assert buffer[ts] == get_pv_value(ts, pv_data)


@patch("pv_sim.combine.get_time")
def test_combiner_fetches_buffer_value(get_time, pv_data):
    ts = get_first_time_stamp(pv_data)
    get_time.return_value = ts

    meter_queue = queue_from_list([MeterValue(time=(ts - 1), meter=2.0)])  # old value

    buffer = {(ts - 1): 2.5}

    logs = combiner(meter_queue, data=pv_data, buffer=buffer)

    assert len(logs) == 1
    assert logs[0] == LogValue(time=ts - 1, pv=2.5, sum=0.5, meter=2.0)


@patch("pv_sim.combine.get_time")
def test_combiner_fetches_buffer_values_for_all_items(get_time, pv_data):
    ts = get_first_time_stamp(pv_data)
    get_time.return_value = ts

    meter_queue = queue_from_list(
        [MeterValue(time=(ts - 2), meter=2.0), MeterValue(time=ts - 1, meter=2.0)]
    )  # old value

    buffer = {(ts - 2): 2.5, (ts - 1): 2.5}

    logs = combiner(meter_queue, data=pv_data, buffer=buffer)

    assert logs[0] == LogValue(time=(ts - 2), pv=2.5, sum=0.5, meter=2.0)
    assert logs[1] == LogValue(time=(ts - 1), pv=2.5, sum=0.5, meter=2.0)

    assert (ts - 2) not in buffer.keys()
    assert (ts - 1) not in buffer.keys()
    assert ts in buffer.keys()


@patch("pv_sim.combine.get_time")
def test_when_newer_meter_values_arrive_older_values_are_also_logged(get_time, pv_data):
    ts = get_first_time_stamp(pv_data)

    get_time.return_value = ts

    meter_queue = queue_from_list([MeterValue(time=ts - 1, meter=2.0)])

    buffer = {
        (
            ts - 2
        ): 2.0,  # older value, with a lost meter value, should get logged as well
        (ts - 1): 2.0,  # matches the new arriving value and should get logged
    }

    logs = combiner(meter_queue, data=pv_data, buffer=buffer)

    assert len(logs) == 2
    assert logs[0].time == ts - 2
    assert logs[1].time == ts - 1


@patch("pv_sim.combine.get_time")
def test_when_buffer_values_are_logged_also_current_value_is_logged(get_time, pv_data):
    ts = get_first_time_stamp(pv_data)

    get_time.return_value = ts

    meter_queue = queue_from_list(
        [MeterValue(time=ts - 1, meter=2.0), MeterValue(time=ts, meter=2.0)]
    )

    buffer = {
        (ts - 1): 2.0,  # matches the new arriving value and should get logged
    }

    logs = combiner(meter_queue, data=pv_data, buffer=buffer)

    assert len(logs) == 2
    assert logs[0].time == ts - 1
    assert logs[1].time == ts


@patch("pv_sim.combine.get_time")
def test_when_buffer_values_are_logged_current_value_is_buffered(get_time, pv_data):
    ts = get_first_time_stamp(pv_data)

    get_time.return_value = ts

    meter_queue = queue_from_list([MeterValue(time=ts - 1, meter=2.0)])

    buffer = {
        (ts - 1): 2.0,  # matches the new arriving value and should get logged
    }

    logs = combiner(meter_queue, data=pv_data, buffer=buffer)

    assert len(logs) == 1
    assert logs[0].time == ts - 1
    assert ts in buffer.keys()
