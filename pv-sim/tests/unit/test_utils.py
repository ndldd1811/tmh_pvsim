from pv_sim.combine import get_time


def test_get_time():
    assert isinstance(get_time(), int)
    assert get_time() >= 0
    assert get_time() <= 24 * 60 * 60  # TODO:generalize


# @patch("pv_sim.app.dt.datetime.now")
# def test_get_time_value(mock):
#     mock.return_value = datetime.datetime(hour=13, min=12)
#     assert get_time() == 0
# from IPython import embed; embed()  # remove_me
