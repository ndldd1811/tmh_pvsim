import datetime
from queue import Queue
from time import sleep

from common.messaging import QUEUE, Message, retry_for_channel, send_stop
from pv_sim.app import MeterListener

from meter.messaging import publish


def test_meter_listener_subscribes_messages():
    testing_host = "127.0.0.1"
    # in the "prod" setup the host uses the docker network name of the rabbit mq service

    ml = MeterListener(queue=Queue(), host=testing_host)
    ml.start()  # start consuming messages
    sent = 0
    consumption_in_message = 1000  # Watt

    with retry_for_channel(host=testing_host) as producer_channel:
        producer_channel.queue_declare(queue=QUEUE)
        while ml.queue.qsize() < 3:
            sleep(0.1)
            m = Message(
                timestamp=datetime.datetime.now(), consumption=consumption_in_message
            )

            publish(producer_channel, m.json())
            consumption_in_message += 1
            sent += 1

        if sent > 4:  # should have stopped by now; stop the test if that failed
            send_stop(producer_channel)
            ml.stop()
            raise AssertionError(
                "Message are published but not added to MeterListener queue"
            )

    ml.stop()
    #  Message.consumption is converted from watt to kW in MeterValue.meter
    assert ml.queue.get().meter == 1.000
    assert ml.queue.get().meter == 1.001
    assert ml.queue.get().meter == 1.002
