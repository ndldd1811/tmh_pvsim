# PV Simulator

Simulates the output of a photovoltaic system installation and combines it with the consumption of a simulated home
meter. The resulting net output is logged together with the original values and a timestamp of the current day.

To start the PV-simulator, Meter-service and the RabbitMQ broker run:

    docker-compose up 

The resulting log file is placed into:

    tail -f log_data/pv-sim.logs

Note: The permissions of the log_dir are set to uid 1000 to match the local (default) user on ubuntu in order to prevent
file permission errors.

# Design

As the Meter service is sending its values via a message queue I assumed a distributed system:

To ensure that the right values are combined, the Meter service sends a timestamp together with the consumption value.
In addition the PvSimulator service buffers PV values for a short period when it does not receive matching home meter
readings. This way the PV values can still be combined with consumption values, when there is a short delay
(e.g. due to temporary connection issues).

## Simulated data

### Home meter data

![mock meter data](images/mock_meter_data.png)

### PV data generated from some selected interpolation points

![mock pv data](images/mock_pv_data.png)

### Resulting PV Simulator output (from plotting log_data with log_reader.ipynb)

![mock sum data](images/plot_log_reader.png)

# Development setup and testing

(tested on Ubuntu 21.04)

- The integration tests depend on a running RabbitMQ service, you can start one with:
```
  docker-compose up -f docker-compose.testing.yml up
  # NOTE: You may have to stop the RabbitMQ service first if it was already started above
```

To setup and test a local development environment then run:
```
  ./dev.sh
  # see comments for more details
```

- The local setup requires `virtualenv` to be installed. Install with `sudo apt install python3-venv` or alternatively
  create a virtualenv with name `venv` manually and run the script.
- To run the demo data notebooks run `jupyterlab`
- Code formatting: `./precommit.sh`


